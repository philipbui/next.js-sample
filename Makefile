help: ## Display this help screen
	grep -h -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

init: ## Initialize project
	npm install -g amdefine
	npm install --save next react react-dom
	yarn

dev: ## Starts the application in development mode (hot-code reloading, error reporting, etc)
	next dev

build: ## Compiles the application for production deployment
	next build

start: build ## Starts the application in production mode.
	NODE_ENV=production
	next start

export: ## Exports the application for production deployment
	next export	

test: ## Tests the application
	NODE_ENV=test
	jest	

codecov: ## Generates CodeCoverage
	jest && codecov	