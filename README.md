# Next JS Sample

[![Build Status](https://travis-ci.org/philip-bui/next.js-sample.svg?branch=master)](https://travis-ci.org/philip-bui/next.js-sample)
[![Dependency Status](https://beta.gemnasium.com/badges/github.com/philip-bui/next.js-sample.svg)](https://beta.gemnasium.com/projects/github.com/philip-bui/next.js-sample)
[![Deploy to now](https://deploy.now.sh/static/button.svg)](https://deploy.now.sh/?repo=https://github.com/philip-bui/next.js-sample)

## Getting Started

Run `make` to get a list of commands.